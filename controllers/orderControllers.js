
const User = require("../models/User");
const Order = require("../models/Order");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// Create Order - Non-admin Only

module.exports.createOrder = async (data) => {

		const userId = data.authId

		let quantity = data.order.products[0].quantity

		let isOrderUpdated = await Product.findById(data.order.products[0].productId).then(toOrder => {

			if (data.isAdmin) {
				// const output = {
				// 	'error!' : "Admins are not allowed to create an order."
				// }
				// return output;
				return Promise.reject();
			} else {
				let productSubtotal = data.order.products[0].quantity * toOrder.price;
				// let orderSubtotal = data.order.products[0].quantity * toOrder.price;

				if (data.order.products.length === 1) {

					orderSubtotal = productSubtotal

					let newOrder = new Order ({
						userId : userId,
						orderSubtotal : orderSubtotal,
						products : [
							{
								productId : data.order.products[0].productId,
								productName : toOrder.name,
								quantity: data.order.products[0].quantity,
								productSubtotal: productSubtotal
							},
						]

					});

					return newOrder.save().then((order, error) => {
						if (error) {
							return false;
						} else {
							return true;
						}	
					});	
				}
			}			
		});

		let isProductUpdated = await Product.findById(data.order.products[0].productId).then(product => {

			let productUpdate = {
					userId : userId,
					quantity : quantity
			}

			product.orders.push(productUpdate);

			product.stocks = (product.stocks - quantity);

			if (product.stocks === 0) {
				product.isListed = false
			}

			return product.save().then((product, error) => {
				if (error){
					return false;
				} else {
					return true;
				};
			});
		});

		if (isOrderUpdated && isProductUpdated) {
			// const output = {
			// 	'alert!' : 'Order succesful!'
			// }
			// return output;
			return true;
		} else {
			return false;
		}	
}


// Retrieve All Orders - Admin Only

module.exports.getAllOrders = (data) => {

	if (data.isAdmin) {
		return Order.find({}).then(allOrders => {
			return allOrders;
		});
	} else {
		// const output = {
		// 	'error!' : 'This feature is limited to admin.'
		// }
		return Promise.reject();
	}
};


// Retrieve User Orders

module.exports.getUserOrders = (data) => {

	if (data.isAdmin) {
		// const output = {
		// 	'error!' : 'This feature is for the authenticated user only.'
		// }
		return Promise.reject();
	} else {
		return Order.find({userId:data.authId}).then(allUserOrders => {
		// const output = {
		// 	'alert!': `Hi, these are your orders`,
		// 	'>' : allUserOrders
		// }
		// return output;
			return allUserOrders;
		});
	}
};

