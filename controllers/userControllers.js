
const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


//Check if the email already exists

module.exports.checkEmailExists = (reqBody) =>{
	return User.find({email:reqBody.email}).then(result=>{
		if(result.length>0){
			return true;
		}else{
			return false;
		};
	});
};


// User Registration

module.exports.registerUser = (data) => {

	return User.find({email:data.email.toLowerCase()}).then(result=>{

		if (result.length>0) {
			const output = {
				'error!' : `Registration failed. Email ${data.email} has already been used`
			}

			return output;

		} else {
			
			let newUser = new User ({

				firstName : data.firstName,
				lastName : data.lastName,
				password : bcrypt.hashSync(data.password,10),
				email : data.email.toLowerCase(),
				mobileNo : data.mobileNo
		
			});

			return newUser.save().then((user,error)=>{
				if (error) {
					return false;
				} else {
					const output = {
							'alert!' : 'Registration successful with the following details:',
							'>' : user
					}
					// return output ;
					return true
				};
			});

		};
	});
};


// User Login

module.exports.loginUser = (reqBody) =>{
	return User.findOne({email:reqBody.email}).then(result=>{
		if (result == null) {
			const output = {
					'error!' : 'No user with this email can be found.',
				}
				// return output;
				return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);

			if (isPasswordCorrect) {
				
				const output = {
					'alert!' : `Now logged in as ${result.firstName} ${result.lastName}. Your access token is:`,
					'>' : `${auth.createAccessToken(result)}`
				}
				// return output;
				return {access:auth.createAccessToken(result), id:result._id, isAdmin:result.isAdmin, email:result.email, firstName:result.firstName, lastName:result.lastName}
			} else {
				const output = {
					'error!' : 'Password is incorrect'
				}
				// return output;
				return false;
			};
		};
	});
};


// Update user as admin

module.exports.updateToAdmin = async (data) => {

    const userToUpdate = await User.findById(data.params.id)
        .then((result, error) => {
            if (error) {
                return 'Error'
            }

            if (result == null) {	
                return null;
            }
            return result
    });

    if (userToUpdate == null) {
        const output  = {
        	'error!': 'User not found.'
        }
        return output
    }	

    if (userToUpdate.isAdmin) {
    	const output = {
    		'error!' : `${userToUpdate.firstName} ${userToUpdate.lastName} is already an admin.`
    	}
        return output
    }

    return User.findByIdAndUpdate(data.params.id).then((result, error) => {
    
        if (error) {
            return 'Update Error'
        }
        result.isAdmin = true;
        return result.save().then((nowAdmin,error)=>{
        	if (error) {
            	return 'Error'
       		} else {
       			const output = {
       				'alert!' : `${result.firstName} ${result.lastName} is now an admin.`,
       				'>' : nowAdmin 
       			}

        		return output;
        	}
        })
    });
}


// Update admin to user

module.exports.updateToUser = async (data) => {

    const userToUpdate = await User.findById(data.params.id)
        .then((result, error) => {
            if (error) {
                return 'Error'
            }

            if (result == null) {	
                return null;
            }
            return result
    });

    if (userToUpdate == null) {
        const output  = {
        	'error!': 'User not found.'
        }
        return output
    }	

    if (!userToUpdate.isAdmin) {
    	const output = {
    		'error!' : `${userToUpdate.firstName} ${userToUpdate.lastName} is already a regular user.`
    	}
        return output
    }

    return User.findByIdAndUpdate(data.params.id).then((result, error) => {
    
        if (error) {
            return 'Update Error'
        }
        result.isAdmin = false;
        return result.save().then((nowUser,error)=>{
        	if (error) {
            	return 'Error'
       		} else {
       			const output = {
       				'alert!' : `${result.firstName} ${result.lastName} is now a regular user.`,
       				'>' : nowUser 
       			}

        		return output;
        	}
        })
    });
}


// Retrieve user details

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
			// result.password = "";
			// console.log(result)
			return result;

	});
};


// Add to cart

module.exports.addToCart = (data, res) => {

	const userId = data.authId;

	if (data.isAdmin) {
		// const output = {
		// 	'error!' : "Admins are not allowed to add items to cart."
		// }
		// return Promise.reject(output);
		return Promise.reject();

	} else {

		return Product.findOne({name: data.product.name}).then((product, err) => {

   			if (product == null) {
   				// const output = {
   				// 	"error!" : 'No product with this name is available.'
   				// }
   				// return output;
   				return false

   			} else {

   				if (data.product.quantity > product.stocks) {
   					// const output = {
   					// 	"error!" : 'Not enough stocks.',
   					// 	">" : `Maximum order as of the moment is limited to ${product.stocks} pieces.`
   					// }
   					// return output;
   					return false

   				} else {

   					const productId = JSON.stringify(product._id).replace(/["]/g, '');

    				let toCart = {
        				productId: productId,
        				productName: product.name,
        				productImage: product.imageUrl,
        				quantity: data.product.quantity,
	        			price: product.price,
	        			subTotal: data.product.quantity * product.price
    				};

    				let cartUpdate = User.findByIdAndUpdate(userId).then(user => {

						let i = user.cart.findIndex(item => item.productId === productId);

						if (i === -1) {
							user.cart.push(toCart);
						} else {

							let newQuantity = user.cart[i].quantity + parseInt(data.product.quantity);

							{(newQuantity > product.stocks) ? newQuantity = product.stocks : newQuantity = newQuantity}

							let toCartUpdate = {
								productId: productId,
    							productName: product.name,
    							productImage: product.imageUrl,
    							quantity: newQuantity,
        						price: product.price,
        						subTotal: newQuantity * product.price
							}

				        	user.cart.splice(i, 1, toCartUpdate)
				        } 

						user.save()
        			});		

    				// const output = {
    				// 	'alert!' : 'Item/s successfully added to cart.',
    				// 	'>' : toCart
    				// }
        			// return output;		 
        			return true;
    				
   				}
   			}	

   		});
   	}
}
   

// View Cart

module.exports.viewCart = (data) => {

	return User.findOne({_id: data.authId}).then(result => {

			// console.log(result.cart)

			let totalAmount = 0;

			result.cart.forEach(item => {

				totalAmount += item.subTotal
			})

			return result.cart;	
			
	});
};


// updating quantity in Cart

module.exports.updateCart = async (data) => {

	const userToUpdate = await User.findOne({_id:data.authId}).then((result, error) => {
        
        	// console.log(data.authId)
        	// console.log(result)   

            if (error) {
                return 'Error'
            }

            if (result == null) {	
                return null;
            }
            return result
    });

    if (userToUpdate == null) {
        const output  = {
        	'error!': 'User not found.'
        }
        return output
    }	

    return User.findByIdAndUpdate(data.authId).then((result, error) => {
    	
        if (error) {
            return 'Update Error'
        }

        {(data.product.quantity < 0) ? quantity = 0 : quantity = data.product.quantity}

        let toUpdate = {
        			quantity: quantity,
	     } 

	        for (let i = 0; i < result.cart.length; i++)

	        if (data.params.productId == result.cart[i].productId) {

	        	result.cart[i].quantity = quantity

	        	result.cart[i].subTotal = quantity * result.cart[i].price
	        }

        return result.save().then((nowUpdated,error) =>  {

        	if (error) {
            	return 'Error'
       		} else {
       			const output = {
       				'alert!' : `Quantity has been succesfully updated`,
       				'>' : nowUpdated.cart 
       			}
        		// return output;
        		return true
        	}
        })
    });

};


// remove From Cart

module.exports.removeFromCart = async (data) => {

	const userToUpdate = await User.findOne({_id:data.authId}).then((result, error) => {
         
            if (error) {
                return 'Error'
            }

            if (result == null) {	
                return null;
            }
            return result
    });

    if (userToUpdate == null) {
        const output  = {
        	'error!': 'User not found.'
        }
        return output
    }

	return User.findByIdAndUpdate(data.authId).then((result, error) => {
    
        if (error) {
            return 'Update Error'
        }

        let toRemove = {
    		productId: data.cart.productId,
		}

	        for (let i = 0; i < result.cart.length; i++)

	        if (data.cart.productId == result.cart[i].productId) {

	        	result.cart.splice(i, 1)

	        }

        return result.save().then((nowRemoved, error) => {

        	if (error) {
            	return 'Error'
       		} else {
       			// const output = {
       			// 	'alert!' : 'Item successfully removed from cart',
       			// 	'>' : nowRemoved.cart 
       			// }
       			// return output;
        		return true;
        	}
        })
    });

};


// Retrieve all users

module.exports.getAllUsers = () => {
	return User.find({}).then(allUsers => {
		return allUsers;
	});
};